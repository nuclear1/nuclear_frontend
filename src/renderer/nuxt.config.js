/**
 * By default, Nuxt.js is configured to cover most use cases.
 * This default configuration can be overwritten in this file
 * @link {https://nuxtjs.org/guide/configuration/}
 */

module.exports = {
  mode: 'spa', // or 'universal'
  head: {
    title: 'nuclear_frontend'
  },
  loading: true,
  plugins: [
    {ssr: true, src: '@/plugins/icons.js'},


  ],
  buildModules: [],
  modules: [
    '@nuxtjs/vuetify',
  ],
  vuetify: {
    theme: {
      options: {
        customProperties: true
      },
      dark: true,
      themes: {
        dark: {
          primary: "#959DCB",
          secondary: "#C792EA",
          accent: '#AB47BC',
          error: '#FF5370',
          info: '#82AAFF',
          success: '#C3E88D',
          warning: '#F3F349',
          background: '#292D3E',
        }
      }
    }
  }
};
