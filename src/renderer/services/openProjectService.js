import axios from 'axios'
import apiConf from '../apiConf'

class OpenProjectService {
  constructor() {
    this.client = axios.create({
      baseURL: apiConf.HOST + apiConf.OPEN_PROJECT
    })
  }

  async list() {
    return await this.client.get("/");
  }
}

export default {
  instance: new OpenProjectService()
}
