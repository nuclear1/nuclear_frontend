import axios from 'axios'
import apiConf from '../apiConf'

class ProjectService {
  constructor() {
    this.client = axios.create({
      baseURL: apiConf.HOST + apiConf.PROJECT
    })
  }

  async list(page, pageSize) {
    return await this.client.get("/?page=" + page + "&page_size=" + pageSize);
  }
}

export default {
  instance: new ProjectService()
}
