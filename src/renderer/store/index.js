export const state = () => ({
  currentProject: undefined,
  openProjects: [],
});

export const mutations = {
  setCurrentProject(state, projectId) {
    state.currentProject = projectId;
  },
  setOpenProject(state, projects) {
    state.openProjects = projects
  },
  addOpenProject(state, project) {
    state.openProjects.push(project);
  },
  removeOpenProject(state, project) {
    state.openProjects.splice(state.openProjects.indexOf(project));
  }
};
